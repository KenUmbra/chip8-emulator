#include "Chip8.h"
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <ios>
#include <vector>

#define CLS_RET_UPPER 0x0000
#define CLS_LOWER 0x0000
#define RET_LOWER 0x000E

#define JMP_ADDR 0x1000
#define CALL 0x2000
#define SE_REG_BYTE 0x3000
#define SNE_REG_BYTE 0x4000
#define SE_REG_REG 0x5000
#define LD_REG_BYTE 0x6000
#define ADD_REG_BYTE 0x7000

#define OPERANDS_UPPER 0x8000
#define LD_REG_REG_LOWER 0x0000
#define OR_LOWER 0x0001
#define AND_LOWER 0x0002
#define XOR_LOWER 0x0003
#define ADD_REG_REG_LOWER 0x0004
#define SUB_LOWER 0x0005
#define SHR_LOWER 0x0006
#define SUBN_LOWER 0x0007
#define SHL_LOWER 0x000E

#define SNE_REG_REG 0x9000
#define LD_INDEX_ADDR 0xA000
#define JMP_OFFSET_ADDR 0xB000
#define RND 0xC000
#define DRW 0xD000

#define SKP_SKNP_UPPER 0xE000
#define SKP_LOWER 0x009E
#define SKNP_LOWER 0x00A1

#define MISC_UPPER 0xF000
#define LD_REG_DELAY_LOWER 0x0007
#define LD_KEY_LOWER 0x000A
#define LD_DELAY_REG_LOWER 0x0015
#define LD_SOUND_REG_LOWER 0x0018
#define ADD_INDEX_REG_LOWER 0x001E
#define LD_F_REG_LOWER 0x0029
#define LD_B_REG_LOWER 0x0033
#define LD_MEMORY_REG_RANGE_LOWER 0x0055
#define LD_REG_RANGE_MEMORY_LOWER 0x0065

#define UPPER_OVERFLOW 0xFF
#define LOWER_OVERFLOW 0x00

#define VF_INDEX 0x0F
#define SPRITE_LENGTH 0x05
#define LINE_LENGTH 0x08

#define ON_COLOR 0xFFFFFFFF
#define OFF_COLOR 0xFF000000

void Chip8::handleOperands()
{
    unsigned char regX = 0, regY = 0;

    switch (this->_opcode & 0x000F)
    {
        case LD_REG_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            this->_vRegs[regX] = this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case OR_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            this->_vRegs[regX] |= this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case AND_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            this->_vRegs[regX] &= this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case XOR_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            this->_vRegs[regX] ^= this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case ADD_REG_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;

            if((this->_vRegs[regX] + this->_vRegs[regY]) > UPPER_OVERFLOW)
            {
                this->_vRegs[VF_INDEX] = 1;
            }
            else
            {
                this->_vRegs[VF_INDEX] = 0;
            }

            this->_vRegs[regX] += this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case SUB_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;

            if(this->_vRegs[regX] > this->_vRegs[regY])
            {
                this->_vRegs[VF_INDEX] = 1;
            }
            else
            {
                this->_vRegs[VF_INDEX] = 0;
            }

            this->_vRegs[regX] -= this->_vRegs[regY];
            this->_programCounter += 2; 
        break;

        case SHR_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;

            if(this->_vRegs[regX] & 0x01)
            {
                this->_vRegs[VF_INDEX] = 1;
            }
            else
            {
                this->_vRegs[VF_INDEX] = 0;
            }

            this->_vRegs[regX] >>= 1;
            this->_programCounter += 2; 
        break;

        case SUBN_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;

            if(this->_vRegs[regY] > this->_vRegs[regX])
            {
                this->_vRegs[VF_INDEX] = 1;
            }
            else
            {
                this->_vRegs[VF_INDEX] = 0;
            }

            this->_vRegs[regX] = this->_vRegs[regY] - this->_vRegs[regX];
            this->_programCounter += 2; 
        break;

        case SHL_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;

            if(this->_vRegs[regX] & 0x80)
            {
                this->_vRegs[VF_INDEX] = 1;
            }
            else
            {
                this->_vRegs[VF_INDEX] = 0;
            }

            this->_vRegs[regX] <<= 1;
            this->_programCounter += 2;
        break;

        default:
            std::cout << "Error: Opcode: " << std::hex << this->_opcode << " does not exist." << std::endl;
        break;
    }
}

void Chip8::handleMisc()
{
    bool keyPress = false;
    unsigned char regX = 0, regY = 0;

    switch (this->_opcode & 0x00FF) 
    {
        case LD_REG_DELAY_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_vRegs[regX] = this->_delayTimer;
            this->_programCounter += 2;
        break;

        case LD_KEY_LOWER:
            keyPress = false;
            
            for(int i = 0; i < 16; ++i)
            {
                regX = (this->_opcode & 0x0F00) >> 8;
                for(int i = 0; i < 16; ++i)
                {
                    if(this->_key[i] != 0x00)
                    {
                        this->_vRegs[regX] = i;
                        keyPress = true;
                    }
                }
            }

            if(!keyPress)						
            {
                break;
            }

            this->_programCounter += 2;
        break;

        case LD_DELAY_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_delayTimer = this->_vRegs[regX];
            this->_programCounter += 2;
        break;

        case LD_SOUND_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_soundTimer = this->_vRegs[regX];
            this->_programCounter += 2;
        break;

        case ADD_INDEX_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_indexRegister += this->_vRegs[regX];
            this->_programCounter += 2;
        break;

        case LD_F_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_indexRegister = this->_vRegs[regX] * SPRITE_LENGTH;
            this->_programCounter += 2;
        break;

        case LD_B_REG_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            this->_memory[this->_indexRegister] = this->_vRegs[regX] / 100;
            this->_memory[this->_indexRegister + 1] = (this->_vRegs[regX] / 10) % 10;
            this->_memory[this->_indexRegister + 2] = (this->_vRegs[regX] % 100) % 10;
            this->_programCounter += 2;
        break;

        case LD_MEMORY_REG_RANGE_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            for(int i = 0; i <= regX; i++)
            {
                this->_memory[this->_indexRegister + i] = this->_vRegs[i];
            }
            this->_programCounter += 2;
        break;

        case LD_REG_RANGE_MEMORY_LOWER:
            regX = (this->_opcode & 0x0F00) >> 8;
            for(int i = 0; i <= regX; i++)
            {
                this->_vRegs[i] = this->_memory[this->_indexRegister + i];
            }
            this->_programCounter += 2;
        break;

        default:
            std::cout << "Error: Opcode: " << std::hex << this->_opcode << " does not exist." << std::endl;
        break;
    }
}

Chip8::Chip8()
{
    this->_programCounter = 0x200;
    this->_opcode = 0;
    this->_indexRegister = 0;
    this->_stackPointer = 0;
    
    std::memset(this->_gfx, OFF_COLOR, sizeof this->_gfx);
    std::memset(this->_stack, 0x00, sizeof this->_stack);
    std::memset(this->_memory, 0x00, sizeof this->_memory);
    std::memset(this->_vRegs, 0x00, sizeof this->_vRegs);
    
    for(int i = 0; i < FONTSET_LENGTH; i++)
    {
        this->_memory[i] = this->FONTSET[i];
    }

    this->_delayTimer = 0;
    this->_soundTimer = 0;
}

Chip8::~Chip8()
{
    
}

bool Chip8::loadGame(std::string gamePath)
{
    std::ifstream gameFile( gamePath, std::ios::binary);
    
    if(gameFile.is_open())
    {
        std::vector<unsigned char> gameData(std::istreambuf_iterator<char>(gameFile), {});
        if(gameData.size() < (MEMORY_SIZE - 0x200))
        {
            for(int i = 0; i < gameData.size() && (i + 0x200) < sizeof this->_memory; i++)
            {
                this->_memory[i + 0x200] = gameData[i];
            }
            return true;
        }
        else
        {
            std::cout << "Error: ROM too big for memory." << std::endl;
        }
        
    }
    return false;
}

void Chip8::emulateCycle()
{
    unsigned int index = 0;
    unsigned short pixel = 0;
    unsigned char regX = 0, regY = 0, kkByte = 0, nibble = 0;

    this->_opcode = this->_memory[this->_programCounter] << 8 | this->_memory[this->_programCounter + 1];
    srand(time(NULL));

    switch(this->_opcode & 0xF000)
    {
        case CLS_RET_UPPER:
            switch (this->_opcode & 0x000F)
            {
                case CLS_LOWER:
                    std::memset(this->_gfx, OFF_COLOR, sizeof this->_gfx);
                    this->_drawFlag = true;
                    this->_programCounter += 2;
                break;

                case RET_LOWER:
                    this->_stackPointer--;
                    this->_programCounter = this->_stack[this->_stackPointer];
                    this->_programCounter += 2;
                break;

                default:
                    std::cout << "Error: Opcode: " << std::hex << this->_opcode << " does not exist." << std::endl;
                break;
            }
        break;

        case JMP_ADDR:
            this->_programCounter = this->_opcode & 0x0FFF;
        break;

        case CALL:
            this->_stack[this->_stackPointer] = this->_programCounter;
            this->_stackPointer++;
            this->_programCounter = this->_opcode & 0x0FFF;
        break;

        case SE_REG_BYTE:
            regX = (this->_opcode & 0x0F00) >> 8;
            kkByte = this->_opcode & 0x00FF;
            
            if(this->_vRegs[regX] == kkByte)
            {
                this->_programCounter += 2;
            }
            this->_programCounter += 2;
        break;

        case SNE_REG_BYTE:
            regX = (this->_opcode & 0x0F00) >> 8;
            kkByte = this->_opcode & 0x00FF;
            
            if(this->_vRegs[regX] != kkByte)
            {
                this->_programCounter += 2;
            }
            this->_programCounter += 2;
        break;

        case SE_REG_REG:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            
            if(this->_vRegs[regX] == this->_vRegs[regY])
            {
                this->_programCounter += 2;
            }
            this->_programCounter += 2;
        break;

        case LD_REG_BYTE:
            regX = (this->_opcode & 0x0F00) >> 8;
            kkByte = this->_opcode & 0x00FF;

            this->_vRegs[regX] = kkByte;
            this->_programCounter += 2;
        break;

        case ADD_REG_BYTE:
            regX = (this->_opcode & 0x0F00) >> 8;
            kkByte = this->_opcode & 0x00FF;

            this->_vRegs[regX] += kkByte;
            this->_programCounter += 2;
        break;

        case OPERANDS_UPPER:
            this->handleOperands();
        break;

        case SNE_REG_REG:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            
            if(this->_vRegs[regX] != this->_vRegs[regY])
            {
                this->_programCounter += 2;
            }
            this->_programCounter += 2;
        break;

        case LD_INDEX_ADDR:
            this->_indexRegister = this->_opcode & 0x0FFF;
            this->_programCounter += 2;
        break;

        case JMP_OFFSET_ADDR:
            this->_programCounter += this->_vRegs[0x00] + (this->_opcode & 0x0FFF);
            this->_programCounter += 2;
        break;

        case RND:
            regX = (this->_opcode & 0x0F00) >> 8;
            kkByte = this->_opcode & 0x00FF;

            this->_vRegs[regX] = (rand() % 0x100) & kkByte;
            this->_programCounter += 2;
        break;

        case DRW:
            regX = (this->_opcode & 0x0F00) >> 8;
            regY = (this->_opcode & 0x00F0) >> 4;
            nibble = this->_opcode & 0x000F;

            this->_vRegs[VF_INDEX] = 0;
            for(int i = 0; i < nibble; i++)
            {
                pixel = this->_memory[this->_indexRegister + i];
                for(int j = 0; j < LINE_LENGTH; j++)
                {
                    index = (this->_vRegs[regX] + j) % SCREEN_WIDTH + ((this->_vRegs[regY] + i) % SCREEN_HEIGHT) * SCREEN_WIDTH;
                    if((pixel & (0x80 >> j)) != 0)
                    {
                        if(this->_gfx[index] == ON_COLOR)
                        {
                            this->_vRegs[VF_INDEX] = 1;
                            this->_gfx[index] = OFF_COLOR;
                        }
                        else
                        {
                            this->_gfx[index] = ON_COLOR;
                        }
                    }
                }
            }

            this->_drawFlag = true;
            this->_programCounter += 2;
        break;

        case SKP_SKNP_UPPER:
            switch (this->_opcode & 0x00FF)
            {
                case SKP_LOWER:
                    regX = (this->_opcode & 0x0F00) >> 8;
                    if(this->_key[this->_vRegs[regX]] == 1)
                    {
                        this->_programCounter += 2;
                    }
                    this->_programCounter += 2;
                break;

                case SKNP_LOWER:
                    regX = (this->_opcode & 0x0F00) >> 8;
                    if(this->_key[this->_vRegs[regX]] == 0)
                    {
                        this->_programCounter += 2;
                    }
                    this->_programCounter += 2;
                break;

                default:
                    std::cout << "Error: Opcode: " << std::hex << this->_opcode << " does not exist." << std::endl;
                break;
            }
        break;

        case MISC_UPPER:
            this->handleMisc();
        break;

        default:
            std::cout << "Error: Opcode: " << std::hex << this->_opcode << " does not exist." << std::endl;
        break;
    }
    
    if(this->_delayTimer > 0)
    {
        this->_delayTimer--;
    }

    if(this->_soundTimer > 0)
    {
        if(this->_soundTimer == 1)
        {
            std::cout << "BEEP" << std::endl;
        }
        this->_soundTimer--;
    }
}

void Chip8::unpressKey(int index)
{
    this->_key[index] = 0x00;
}

void Chip8::pressKey(int index)
{
    this->_key[index] = 0x01;
}

void Chip8::setDrawFlag(const bool drawFlag)
{
    this->_drawFlag = drawFlag;
}

bool Chip8::getDrawFlag() const
{
    return this->_drawFlag;
}

unsigned int* Chip8::getGfx()
{
    return this->_gfx;
}