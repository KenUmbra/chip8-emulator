COMPILER = g++
FLAGS = -w -lSDL2

HEADERS = Chip8.h
SRC = main.cpp Chip8.cpp
EXECUTABLE = chip8emu

OBJECTS = $(SRC:.cpp=.o)

default: $(EXECUTABLE)

%.o: %.cpp
	$(COMPILER) -c -o $@ $<

$(EXECUTABLE): $(OBJECTS) $(HEADERS)
	$(COMPILER) $(OBJECTS) $(FLAGS) -o $@

clean:
	-rm -f $(OBJECTS)
	-rm -f $(EXECUTABLE)

run: $(EXECUTABLE)
	./$(EXECUTABLE)