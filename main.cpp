#include <SDL2/SDL_render.h>
#include <SDL2/SDL_timer.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL.h>
#include "Chip8.h"

#define REQUIRED_FIELDS 2
#define FRAMES_PER_SECOND 60
#define MILLISECONDS_PER_CYCLE 1000 / FRAMES_PER_SECOND
#define SCALE 10

bool handleInput(SDL_Event, Chip8*);
void drawGraphics(Chip8&, SDL_Renderer*, SDL_Texture*);

int main(int argc, char* argv[]) 
{
  SDL_Window* window = nullptr;
  SDL_Renderer* renderer = nullptr;
  SDL_Texture* texture = nullptr;
  SDL_Event event;
  Chip8 chip8Emu;
  unsigned int startTick = 0, frameSpeed = 0;
  bool isRunning = true;

  if(REQUIRED_FIELDS != argc)
  {
    std::cout << "Usage: " << argv[0] << " /path/to/game" << std::endl;
    return 1;
  }

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    std::cout << "Error: Could not initialize SDL2" << std::endl;
    return 1;
  }
  
  window = SDL_CreateWindow("Ken's Chip8Emu", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH * SCALE, SCREEN_HEIGHT * SCALE, SDL_WINDOW_SHOWN);
  if(window == nullptr)
  {
    std::cout << "Error: Could not initialize window" << std::endl;
    return 1;
  }
  
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, SCREEN_WIDTH, SCREEN_HEIGHT);

  
  if(!chip8Emu.loadGame(argv[1]))
  {
    return 1;
  }
 
  while(isRunning)
  {
    startTick = SDL_GetTicks();

    chip8Emu.emulateCycle();
 
    if(chip8Emu.getDrawFlag())
    {
      drawGraphics(chip8Emu, renderer, texture);
      chip8Emu.setDrawFlag(false);
    }
    
    isRunning = handleInput(event, &chip8Emu);
    frameSpeed = SDL_GetTicks() - startTick;
    if (frameSpeed < MILLISECONDS_PER_CYCLE) {
      SDL_Delay(MILLISECONDS_PER_CYCLE - frameSpeed);
    }
  }
 
  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}

/*
This function handles user input.
input: The SDL Event (SDL_Event), pointer to the Chip8 Emulator (Chip8*). 
output: true - the user did not close the emulator, false - the user closed the emulator (bool).
*/
bool handleInput(SDL_Event event, Chip8* chip8Emu)
{
  while (SDL_PollEvent(&event) != 0)
  {  
    if(event.type == SDL_KEYDOWN)
    {
      switch (event.key.keysym.sym)
      {
        case SDLK_ESCAPE:
          return false;
        break;

        case SDLK_1:
          chip8Emu->pressKey(0x01);
        break;

        case SDLK_2:
          chip8Emu->pressKey(0x02);
        break;

        case SDLK_3:
          chip8Emu->pressKey(0x03);
        break;
        
        case SDLK_4:
          chip8Emu->pressKey(0x0C);
        break;

        case SDLK_q:
          chip8Emu->pressKey(0x04);
        break;

        case SDLK_w:
          chip8Emu->pressKey(0x05);
        break;

        case SDLK_e:
          chip8Emu->pressKey(0x06);
        break;

        case SDLK_r:
          chip8Emu->pressKey(0x0D);
        break;

        case SDLK_a:
          chip8Emu->pressKey(0x07);
        break;

        case SDLK_s:
          chip8Emu->pressKey(0x08);
        break;

        case SDLK_d:
          chip8Emu->pressKey(0x09);
        break;

        case SDLK_f:
          chip8Emu->pressKey(0x0E);
        break;

        case SDLK_z:
          chip8Emu->pressKey(0x0A);
        break;

        case SDLK_x:
          chip8Emu->pressKey(0x00);
        break;

        case SDLK_c:
          chip8Emu->pressKey(0x0B);
        break;
        
        case SDLK_v:
          chip8Emu->pressKey(0x0F);
        break;
      }
    }
    else if(event.type == SDL_KEYUP)
    {
      switch (event.key.keysym.sym)
      {
        case SDLK_1:
          chip8Emu->unpressKey(0x01);
        break;

        case SDLK_2:
          chip8Emu->unpressKey(0x02);
        break;

        case SDLK_3:
          chip8Emu->unpressKey(0x03);
        break;
        
        case SDLK_4:
          chip8Emu->unpressKey(0x0C);
        break;

        case SDLK_q:
          chip8Emu->unpressKey(0x04);
        break;

        case SDLK_w:
          chip8Emu->unpressKey(0x05);
        break;

        case SDLK_e:
          chip8Emu->unpressKey(0x06);
        break;

        case SDLK_r:
          chip8Emu->unpressKey(0x0D);
        break;

        case SDLK_a:
          chip8Emu->unpressKey(0x07);
        break;

        case SDLK_s:
          chip8Emu->unpressKey(0x08);
        break;

        case SDLK_d:
          chip8Emu->unpressKey(0x09);
        break;

        case SDLK_f:
          chip8Emu->unpressKey(0x0E);
        break;

        case SDLK_z:
          chip8Emu->unpressKey(0x0A);
        break;

        case SDLK_x:
          chip8Emu->unpressKey(0x00);
        break;

        case SDLK_c:
          chip8Emu->unpressKey(0x0B);
        break;
        
        case SDLK_v:
          chip8Emu->unpressKey(0x0F);
        break;
      }
    }
  }
  return true;
}

/*
This function renders the grphaics onto the screen.
input: Reference to the Chip8 Emulator (Chip8&), pointer to the SDL Renderer (SDL_Renderer*), pointer to the SDL Texture (SDL_Texture*).
output: None.
*/
void drawGraphics(Chip8& chip8Emu, SDL_Renderer* renderer, SDL_Texture* texture)
{
  unsigned int* gfx = chip8Emu.getGfx();
  SDL_UpdateTexture(texture, NULL, gfx, SCREEN_WIDTH * sizeof(unsigned int));
  SDL_RenderCopy(renderer, texture, NULL, NULL);
  SDL_RenderPresent(renderer);
  SDL_Delay(0);
}