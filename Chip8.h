#pragma once
#include <string>

#define FONTSET_LENGTH 0X50
#define STACK_SIZE 0x10
#define SCREEN_WIDTH 0x40
#define SCREEN_HEIGHT 0x20
#define MEMORY_SIZE 0x1000
#define AMOUNT_OF_REGISTERS 0x10
#define AMOUNT_OF_KEYS 0x10

class Chip8
{
private:
    const unsigned char FONTSET[FONTSET_LENGTH] =
    { 
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    };


    unsigned int _gfx[SCREEN_WIDTH * SCREEN_HEIGHT];

    unsigned short _stack[STACK_SIZE];

    unsigned char _memory[MEMORY_SIZE];
    unsigned char _vRegs[AMOUNT_OF_REGISTERS];
    unsigned char _key[AMOUNT_OF_KEYS];
    
    unsigned short _indexRegister;
    unsigned short _programCounter;
    unsigned short _stackPointer;
    unsigned short _opcode;
    
    unsigned char _delayTimer;
    unsigned char _soundTimer;

    bool _drawFlag;

    /*
    This function handles operand opcodes.
    input: None.
    output: None.
    */
    void handleOperands();
    
    /*
    This function handles misc opcodes.
    input: None.
    output: None.
    */
    void handleMisc();

public:
    
    /*
    This function constructs the emulator.
    input: None.
    output: None.
    */
    Chip8();

    /*
    This function destructs the emulator.
    input: None.
    output: None.
    */
    ~Chip8();
    
    /*
    This function loads a game into the emulator.
    input: The path to the game (std::string).
    output: None.
    */
    bool loadGame(std::string);
    
    /*
    This function emulates a CHIP8 cycle.
    input: None.
    output: None.
    */
    void emulateCycle();
    
    /*
    This function sets a pressed key.
    input: The index of the pressed key.
    output: None.
    */
    void pressKey(int);

    /*
    This function unsets a pressed key.
    input: The index of the unpressed key.
    output: None.
    */
    void unpressKey(int);
    
    /*
    This function sets the draw flag.
    input: The emulator's draw flag (bool).
    output: None.
    */
    void setDrawFlag(const bool);

    /*
    This function fetches the draw flag.
    input: None.
    output: The emulator's draw flag (bool).
    */
    bool getDrawFlag() const;

    unsigned int* getGfx();
};