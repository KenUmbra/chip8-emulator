# Chip8 Emulator

## Name
Ken's Chip-8 Emulator

## Description
My own version of a Chip-8 Interpeter & Emulator.

## Badges
--

## Visuals
--

## Installation
1. Make sure you have both SDL2 & Make installed.
2. Clone the repository.
```
git clone https://gitlab.com/KenUmbra/chip8-emulator.git
```
3. Windows users only, remove the `SDL2/` prefix from the relevant libraries or you might run into compilation issues.
4. Compile the program with make.
5. Enjoy!


## Usage
Play .ch8 files, aka CHIP8 games.

## Support
Go to stackoverflow or something.

## Roadmap
--

## Contributing
--

## Authors and acknowledgment
I have made this on my own.
However, I wouldn't have been abale to compile any of my files if it weren't for my friend Alon (I'm really bad with makefiles). So kudos to him!
I also wish to thank the emulation community for the amount of sources they made for learning about CHIP8 emulation.

## License
GPL3.

## Project status
Complete.
